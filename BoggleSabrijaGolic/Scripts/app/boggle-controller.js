﻿angular.module("boggleApp")
    .controller('BoggleCtrl', function ($scope, $http) {
        var self = this;
        self.possibleLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        self.foundWord = "";
        self.foundWords = [];
        self.rowList = [[], [], [], []];
        self.pressedButtons = [];
        self.SetRandomLetters = function () {
            for (var i = 0; i < 4; i++) {
                for (var j = 0; j < 4; j++) {
                    var index = Math.floor(Math.random() * self.possibleLetters.length);
                    if (self.possibleLetters.charAt(index) == 'Q') {
                        self.rowList[i][j] = { value: 'Qu', x: i, y: j, disabled: false, color: "white" };
                    } else {
                        self.rowList[i][j] = { value: self.possibleLetters.charAt(index), x: i, y: j, disabled: false, color: "white" };
                    }
                    self.possibleLetters = self.possibleLetters.replace(self.possibleLetters[index], '');
                }
            }
        };
        self.SetRandomLetters();
        self.FinishWord = function () {
            if (self.foundWord.length > 2) {
                if (self.foundWords.includes(self.foundWord)) {
                    self.foundWord = "";
                    self.pressedButtons = [];
                    self.ResetBoard();
                } else {
                    self.foundWords.push(self.foundWord);
                    self.foundWord = "";
                    self.pressedButtons = [];
                    self.ResetBoard();
                }
            } else {
                window.alert("Would shound not be shorter than 3 letters")
            }
        };
        self.GetResult = function () {
            console.log(self.foundWord);
            $http.post("http://localhost:58730/api/Boggle/ReturnScore", self.foundWords).then(function mySuccess(response) {
                window.alert("Your final result is: " + response.data);

            });

            self.possibleLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            self.foundWord = "";
            self.foundWords = [];
            self.rowList = [[], [], [], []];
            self.pressedButtons = [];
            self.SetRandomLetters();
        };
        self.CancelWord = function () {
            self.foundWord = "";
            self.pressedButtons = [];
            self.ResetBoard();
        };
        self.SubmitLetter = function (x, y, letter) {
            self.foundWord += letter;
            self.pressedButtons.push([x, y]);
            self.ResetBoard();
            for (var i = 0; i < self.pressedButtons.length; i++) {
                self.rowList[self.pressedButtons[i][0]][self.pressedButtons[i][1]].disabled = true;
                self.rowList[self.pressedButtons[i][0]][self.pressedButtons[i][1]].color = "green";
            }

            for (var i = x - 2; i > -1; i--) {
                for (var j = 0; j < 4; j++) {
                    self.rowList[i][j].disabled = true;
                }
            }

            for (var i = x + 2; i < 4; i++) {
                for (j = 0; j < 4; j++) {
                    self.rowList[i][j].disabled = true;
                }
            }

            for (var j = y - 2; j > -1; j--) {
                for (var i = 0; i < 4; i++) {
                    self.rowList[i][j].disabled = true;
                }
            }

            for (var j = y + 2; j < 4; j++) {
                for (var i = 0; i < 4; i++) {
                    self.rowList[i][j].disabled = true;
                }
            }
        };
        self.ResetBoard = function () {
            for (var i = 0; i < 4; i++) {
                for (var j = 0; j < 4; j++) {
                    self.rowList[i][j].disabled = false;
                    self.rowList[i][j].color = "white";

                }
            }
        };



    });
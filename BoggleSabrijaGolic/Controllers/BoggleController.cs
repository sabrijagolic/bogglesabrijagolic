﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BoggleSabrijaGolic.Controllers
{
    public class BoggleController : ApiController
    {
        //api/Boggle/ReturnScore
        [HttpPost]
        public int ReturnScore([FromBody] List<string> listOfStrings) //First part - Word List Score Function
        {
            return CalculateScore(listOfStrings);

        }

        //api/Boggle/ReturnScoreForAllPlayers
        [HttpPost]
        public List<int> ReturnScoreForAllPlayers([FromBody] List<List<string>> listOfAllPlayersResults) //First Part - Multiplayer Score Function
        {
            List<List<string>> uniqueAnswers = new List<List<string>>();
            List<int> endScores = new List<int>();
            for (int i = 0; i < listOfAllPlayersResults.Count; i++)
            {
                var currentList = listOfAllPlayersResults[i];
                for (int j = 0; j < listOfAllPlayersResults.Count; j++)
                {
                    if (i != j)
                    {
                        currentList = currentList.Except(listOfAllPlayersResults[j]).ToList();
                    }
                }
                uniqueAnswers.Add(currentList);
            }
            foreach (List<string> list in uniqueAnswers)
            {
                endScores.Add(CalculateScore(list));
            }

            return endScores;
        }

        private int CalculateScore(List<string> listOfStrings)
        {
            int endScore = 0;
            foreach (string word in listOfStrings)
            {
                if (word.Length > 2)
                {
                    switch (word.Length)
                    {
                        case 3:
                            endScore += 1;
                            break;
                        case 4:
                            endScore += 1;
                            break;
                        case 5:
                            endScore += 2;
                            break;
                        case 6:
                            endScore += 3;
                            break;
                        case 7:
                            endScore += 5;
                            break;
                        default:
                            endScore += 11;
                            break;
                    };
                }
            }
            return endScore;
        }




    }
}
